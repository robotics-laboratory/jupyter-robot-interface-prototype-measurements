# Robot interface measurements

There is data representing various velocities and accelerations of the same linear-rotational motion in `Velocities and Accelerations.ipynb`.

There is data representing various velocities with motion along linear path with zero rotation in `Velocities.ipynb`.
